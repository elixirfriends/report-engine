package io.bunsan.jinterface;

import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangPid;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;
import com.ericsson.otp.erlang.OtpMbox;
import com.ericsson.otp.erlang.OtpNode;

import org.apache.log4j.Logger;

/**
 * Ejemplo del libro: Erlang and OTP in Action, capitulo 13.
 *
 */
public class JInterfaceExample {
	private static final Logger LOG = Logger.getLogger(JInterfaceExample.class);

	private OtpNode node;
	private OtpMbox mbox;

	public JInterfaceExample(String nodeName, String mboxName, String cookie) throws Exception {
		super();
		node = new OtpNode(nodeName, cookie);
		mbox = node.createMbox(mboxName);
		
		for (String name : node.getNames()) {
			LOG.debug("Name: " + name);
		}
	}

	public static void main(String[] args) throws Exception {
		if (args.length != 3) {
			LOG.error("wrong number of arguments");
			LOG.error("expected: nodeName mailboxName cookie");
			return;
		}
		JInterfaceExample ex = new JInterfaceExample(args[0],args[1],args[2]);
		ex.process();
	}

	private void process() {
		while (true) {
			try {
				OtpErlangObject msg = mbox.receive();
				OtpErlangTuple t = (OtpErlangTuple) msg;
				
				OtpErlangPid from = (OtpErlangPid) t.elementAt(0);
				LOG.debug("from: " + from);
				
				String name = ((OtpErlangString) t.elementAt(1)).stringValue();
				LOG.debug("name: " + name);
				
				String greeting = "Greetings from Java, " + name + "!";
				OtpErlangString replystr = new OtpErlangString(greeting);
				
				OtpErlangTuple outMsg = new OtpErlangTuple(new OtpErlangObject[] { mbox.self(), replystr });
				mbox.send(from, outMsg);
			} catch (Exception e) {
				LOG.error("Caught error at process method: ", e);
			}
		}
	}

}
