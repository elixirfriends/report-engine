package io.bunsan.jinterface.node;

import org.apache.log4j.Logger;

/**
 * Main Java class ReportNode—For the Java node. Much like the example code in
 * section 13.1.4, it handles incoming requests from the Erlang side and
 * dispatches them.
 */
public class ReportNode {
	private static final Logger LOG = Logger.getLogger(ReportNode.class);
	
	public static void main(String[] args) {
		LOG.debug("Hello World!");
	}
}
