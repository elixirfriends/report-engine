package io.bunsan.jinterface.task;

import org.apache.log4j.Logger;

/**
 * Java class ReportTask—Handles each request in its own thread. 
 * Uses ReportConnector to perform the requested function,
 * and sends a reply back to the Erlang client.
 */
public class PoolTask {
	private static final Logger LOG = Logger.getLogger(PoolTask.class);

}
